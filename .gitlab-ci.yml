workflow:
  rules:
    - if: $CI_COMMIT_BRANCH
    - if: '$CI_PIPELINE_SOURCE == "schedule"'

stages:
  - build-ci-container-stage
#  - test-hipsycl-in-ci-container-stage
  - build-in-ci-container-stage
#  - test-gromacs-in-ci-container-stage

.variables:
  variables:
    ROCM_VERSION: 4.0.1
    ROCM_CLANG_PATH: /opt/rocm-$ROCM_VERSION/llvm/bin
#    HIPSYCL_REF_SLUG: 9867a7b # from develop branch
    HIPSYCL_REF_SLUG: 0bf6420aab18c4526f9ad50867c3c50f171db9ea # from develop branch
    GROMACS_REF_SLUG: master
    IMAGE_TAG: $CI_REGISTRY_IMAGE:rocm-$ROCM_VERSION-hipsycl-$HIPSYCL_REF_SLUG-gromacs-$GROMACS_REF_SLUG
    LATEST_TAG: $CI_REGISTRY_IMAGE:latest

build-ci-container:
  extends:
    - .variables
  image: docker:20
  services:
    # Published by the above image
    - docker:20-dind
  stage: build-ci-container-stage
  when: manual
  script:
    - set -x
    # Ensure docker can re-use old image layers where available
#    - echo "$CI_REGISTRY_PASSWORD" | docker login --username $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
#    - docker pull $LATEST_TAG > /dev/null && echo "Pulled latest image" || echo "No image to pull"
#    - docker pull $IMAGE_TAG > /dev/null && echo "Pulled image from this branch" || echo "No old image from this branch to pull"
    # Clone GROMACS master to build a container like it uses for CI.
    # The docker-in-docker image used by GitLab shared runners is based
    # on alpine linux. We install python3, pip, git, and hpccm onto it.
    - apk add --update python3 py3-pip git
    - python3 -m pip install --upgrade pip hpccm
    # Then we do a shallow clone of GROMACS master
    - git clone https://gitlab.com/gromacs/gromacs.git --single-branch --branch master --depth=1
    # Finally build the container
    - (cd gromacs/admin/containers; python3 scripted_gmx_docker_builds.py
       --llvm 11
       --rocm $ROCM_VERSION
       --hipsycl $HIPSYCL_REF_SLUG
       --cmake 3.16.3
       > ../../../Dockerfile)
    - cat Dockerfile
    - docker build -t $IMAGE_TAG .
    # Ensure the docker login has not timed out during the build
    - echo "$CI_REGISTRY_PASSWORD" | docker login --username $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    # Push the binder image to the registry so it can be re-used
    - docker push $IMAGE_TAG

build-gromacs-in-ci-container:
  extends:
    - .variables
  image: $IMAGE_TAG
  stage: build-in-ci-container-stage
  when: always
  variables:
    CMAKE: /usr/local/cmake-3.16.3/bin/cmake
    KUBERNETES_CPU_LIMIT: 12
    KUBERNETES_CPU_REQUEST: 8
    KUBERNETES_MEMORY_REQUEST: 16Gi
  tags:
    - k8s-scilifelab
  script:
    - set -x
    - git clone https://gitlab.com/gromacs/gromacs.git --single-branch --branch $GROMACS_REF_SLUG --depth=1
    - mkdir -p gromacs/build
    - cd gromacs/build
    - $CMAKE .. -G Ninja
      -DGMX_SIMD=AVX2_256 -DCMAKE_BUILD_TYPE=RelWithAssert
      -DGMX_GPU=SYCL -DGMX_SYCL_HIPSYCL=ON
      -DHIPSYCL_TARGETS='hip:gfx906'
      -DCMAKE_C_COMPILER=$ROCM_CLANG_PATH/clang
      -DCMAKE_CXX_COMPILER=$ROCM_CLANG_PATH/clang++
    - $CMAKE --build . -- -j$KUBERNETES_CPU_LIMIT 2>&1 | tee buildLogFile.log
    - $CMAKE --build . --target tests -- -j$KUBERNETES_CPU_LIMIT 2>&1 | tee testBuildLogFile.log
    - awk '/warning/,/warning.*generated|^$/' buildLogFile.log testBuildLogFile.log | grep -v "CMake" | tee buildErrors.log || true
    - grep "cannot be built" buildLogFile.log testBuildLogFile.log | tee -a buildErrors.log || true
    - $CMAKE --build . --target install 2>&1 | tee installBuildLogFile.log
    - if [ -s buildErrors.log ] ; then echo "Found compiler warning during build"; cat buildErrors.log; exit 1; fi
    - ctest -D ExperimentalTest --output-on-failure | tee ctestLog.log || true
    - awk '/The following tests FAILED/,/^Errors while running CTest|^$/' ctestLog.log | tee ctestErrors.log
    - xsltproc $CI_PROJECT_DIR/scripts/CTest2JUnit.xsl Testing/`head -n 1 < Testing/TAG`/Test.xml > JUnitTestResults.xml
    - if [ -s ctestErrors.log ] ; then
      echo "Error during running ctest";
      exit 1;
      fi
    - $CMAKE --build . target install
    - cd ..
  artifacts:
    paths:
      - /usr/local/gromacs

build-sycl-bench-in-ci-container:
  extends:
    - .variables
  image: $IMAGE_TAG
  stage: build-in-ci-container-stage
  when: always
  variables:
    CMAKE: /usr/local/cmake-3.16.3/bin/cmake
    KUBERNETES_CPU_LIMIT: 8
    KUBERNETES_CPU_REQUEST: 4
    KUBERNETES_MEMORY_REQUEST: 8Gi
    SYCL_BENCH_REF_SLUG: master
  script:
    - set -x
    - git clone https://github.com/bcosenza/sycl-bench.git --single-branch --branch $SYCL_BENCH_REF_SLUG --depth=1
    - mkdir -p sycl-bench/build
    - cd sycl-bench/build
    - $CMAKE .. 
      -G Ninja
      -DCMAKE_CXX_COMPILER=$ROCM_CLANG_PATH/clang++
      -DSYCL_IMPL=hipSYCL -DHIPSYCL_TARGETS='hip:gfx906'
    - $CMAKE --build . -- -j$KUBERNETES_CPU_LIMIT

